#include <msp430.h> 

/*
 * main.c
 */
int main(void) {
	WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer

	      	P2DIR |= BIT1;                // TA1CCR1 on P2.1
	        P2SEL |= BIT1;
//	        P2SEL &= ~BIT1;				// TA1CCR1 on P2.1
	        P2OUT &= ~BIT1;

	        P2DIR |= BIT4;                // TA1CCR2 on P2.4
	        P2SEL |= BIT4;                // TA1CCR2 on P2.4
//	        P2SEL2 &= ~BIT4;
	        P2OUT &= ~BIT4;

	        P2DIR |= BIT2|BIT3;		//sets up 3 GPIO (1.2 is enable, 2.3 is left direction, 2.2 is right direction)
	        P2SEL &= ~ (BIT2|BIT3);
	        P2OUT &= ~ (BIT2|BIT3);

	        P1DIR |= BIT2;
	        P1OUT &= ~BIT2;

	        TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK


	        TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
	        TA1CCR1 = 510;                // set duty cycle to 50%
	        TA1CCTL1 |= OUTMOD_7;        // set TACCTL1 to Set / Reset mode

	        TA1CCR2 = 500;                // set duty cycle to 50%
	       	TA1CCTL2 |= OUTMOD_7;        // set TACCTL2 to Set / Reset mode

	        while (1) {

	        	// Forward
	            __delay_cycles(3000000);
	            P1OUT |= BIT2;  //set enable
	            __delay_cycles(5000000);
	            P1OUT &= ~ BIT2;

	            //backward

	            P1OUT |= BIT2;  //set enable
	            P2OUT |= BIT2|BIT3;
	            __delay_cycles(5000000);
	            P2OUT &= ~ (BIT2|BIT3);
	            P1OUT &= ~ BIT2;

//	            //slight left
//
	            __delay_cycles(1000000);
	           	P2OUT |= BIT3;
	            P1OUT |= BIT2;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	           	__delay_cycles(50000);
	           	P2OUT &= ~ BIT3;
	           	P1OUT &= ~ BIT2;


//
	            //slight right

	           	__delay_cycles(1000000);
	           	P2OUT |= BIT2;
	            P1OUT |= BIT2;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	           	__delay_cycles(50000);
	           	P2OUT &= ~ BIT2;
	           	P1OUT &= ~ BIT2;

	            //90 left
	           	__delay_cycles(1000000);
	           	P2OUT |= BIT2;
	            P1OUT |= BIT2;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	           	__delay_cycles(1000000);
	           	P2OUT &= ~ (BIT2|BIT3);
	           	P1OUT &= ~ BIT2;

	            //90 right
	           	P2OUT |= BIT3;
	            P1OUT |= BIT2;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	           	__delay_cycles(1000000);
	           	P2OUT &= ~ (BIT2|BIT3);
	           	P1OUT &= ~ BIT2;

	}
}
