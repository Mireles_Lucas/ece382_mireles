/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: 17 Nov 16
Course: ECE 382
File: lab6.c
Event: Lab 6

Purp: source fule that contains all the actual functions being called by main

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/


#include <msp430.h>
#include "Lab6.h"

void required_functionality(void){ //function that calls functions to show required functionality
	forward();
	backward();
	left();
	right();
	left90();
	right90();
	return;
}


void forward(){ //moves the robot forward
	// Forward

	P1OUT |= BIT2;  //set enable
	__delay_cycles(1000000);
	P1OUT &= ~ BIT2;
	return;
}
void backward(){ //moves the robot backward
	TA1CCTL1 |= OUTMOD_3;
	P1OUT |= BIT2;  //set enable
	P2OUT |= BIT2|BIT3;
	__delay_cycles(1000000);
	P2OUT &= ~ (BIT2|BIT3);
	P1OUT &= ~ BIT2;
	TA1CCTL1 |= OUTMOD_7;
	return;
}
void left(){ //moves the robot slightly left
	__delay_cycles(1000000);
	P2OUT |= BIT3;
	P1OUT |= BIT2;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	__delay_cycles(50000);
	P2OUT &= ~ BIT3;
	P1OUT &= ~ BIT2;
	return;
}
void right(){ //moves the robot slighty right
	__delay_cycles(1000000);
	P2OUT |= BIT2;
	P1OUT |= BIT2;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	__delay_cycles(50000);
	P2OUT &= ~ BIT2;
	P1OUT &= ~ BIT2;
	return;
}
void right90(){ //moves the robot 90 degrees to the right
	//90 left
	__delay_cycles(1000000);
	P2OUT |= BIT2;
	P1OUT |= BIT2;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	__delay_cycles(250000);
	P2OUT &= ~ (BIT2|BIT3);
	P1OUT &= ~ BIT2;
	return;
}
void left90(){ //moves the robot 90 degrees to the left
	 //90 right
	P2OUT |= BIT3;
	P1OUT |= BIT2;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	__delay_cycles(250000);
	P2OUT &= ~ (BIT2|BIT3);
	P1OUT &= ~ BIT2;
	return;
}
void initMSP430(void){
	WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer

		      	P2DIR |= BIT1;                // TA1CCR1 on P2.1
		        P2SEL |= BIT1;
	//	        P2SEL &= ~BIT1;				// TA1CCR1 on P2.1
		        P2OUT &= ~BIT1;

		        P2DIR |= BIT4;                // TA1CCR2 on P2.4
		        P2SEL |= BIT4;                // TA1CCR2 on P2.4
	//	        P2SEL2 &= ~BIT4;
		        P2OUT &= ~BIT4;

		        P2DIR |= BIT2|BIT3;		//sets up 3 GPIO (1.2 is enable, 2.3 is left direction, 2.2 is right direction)
		        P2SEL &= ~ (BIT2|BIT3);
		        P2OUT &= ~ (BIT2|BIT3);

		        P1DIR |= BIT2;
		        P1OUT &= ~BIT2;

		        P2DIR &= ~BIT6;						// Set up P2.6 as GPIO not XIN
				P2SEL &= ~BIT6;						// Once again, this take three lines
				P2SEL2 &= ~BIT6;					// to properly do

				P2IFG &= ~BIT6;						// Clear any interrupt flag on P2.6
				P2IE  |= BIT6;						// Enable P2.6 interrupt

				HIGH_2_LOW;							// check the header out.  P2IES changed.
				P1DIR |= BIT0|BIT6;					// Set LEDs as outputs
				P1OUT &= ~(BIT0|BIT6);				// And turn the LEDs off

				TA0CCR0 = 16000;					// create a 16ms roll-over period
				TA0CTL &= ~TAIFG;					// clear flag before enabling interrupts = good practice
				TA0CTL |= ID_0|TASSEL_2|MC_1|TAIE;	// Use 1:8 prescalar off SMCLK and enable interrupts
				HIGH_2_LOW;

				_enable_interrupt();

		        TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK


		        TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
		        TA1CCR1 = 510;                // set duty cycle to 50%
		        TA1CCTL1 |= OUTMOD_7;        // set TACCTL1 to Set / Reset mode

		        TA1CCR2 = 500;                // set duty cycle to 50%
		       	TA1CCTL2 |= OUTMOD_7;        // set TACCTL2 to Set / Reset mode

		        P1DIR |= BIT6;						//
		       	P1SEL |= BIT6;						// Once again, this take three lines
		       	P1SEL2 &= ~BIT6;					// to properly do
		       	P1OUT  &= ~BIT6;

		       	return;
}

void servo(){
				// MOVE Right

				TA0CCR0 = 20000;					// create a 20ms roll-over period
				TA0CCR1 = 500;
				TA0CCTL1 |= OUTMOD_7;
				P1OUT |= BIT6;
				__delay_cycles(1000000);
				TA0CCR1 = 1500;
				__delay_cycles(1000000);
				TA0CCR1 = 2500;
				__delay_cycles(1000000);
				return;
}
