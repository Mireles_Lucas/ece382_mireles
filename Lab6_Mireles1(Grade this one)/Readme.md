# Lab 6 - PWM ROBOT - "Robot Motion"

## By C2C Lucas Mireles

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
3. [Hardware schematic](#hardware-schematic)
4. [Debugging](#debugging)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Objectives or Purpose 
This lab is designed to provide you with experience using the pulse-width modulation features of the MSP430. You will need to program the MSP430 to generate pulse-width-modulated waveforms to control the speed / direction of your robot's motors. In this lab, you will make your robot move forward, backwards, a small (< 45 degree) turn left/right, and a large (> 45 dgree) turn left/right..

### Prelab

Consider your hardware (timer subsystems, chip pinout, etc.) and how you will use it to achieve robot control. Which pins will output which signals you need? Which side of the motor will you attach these signals to? How will you use these signals to achieve forward / back / left / right movement? 

-I will be using pins 2.0, 2.1, 2.2, 2.3, 2.4. Pins 2.1 and 2.4 will be my PWM for the left and right motor respectively. Pin 2.0 will be enable pin that connects to the motor chip. Pins 2.2 and 2.3 will be my GPIO for my left and right motor respectively. I will be able to achieve forward and left movement by connecting the positive side of the left motor to P2.1 (Left PWM) and the negative side of he right motor to P2.4 (Right PWM) in order to make the GPIO inputs the same when trying to make the robot go the same direction. More info on how to change direction can be found in the flow chart. 

Consider how you will setup the PWM subsytem to achieve this control. What are the registers you'll need to use? Which bits in those registers are important? What's the initialization sequence you'll need?

- I will use TA1CTL1 to set up the left PWM and make its OUTMODE = 3. I will use TA1CTL 2 to set up the Right PWM and make its OUTMODE = 3.  Initially I will make both TA1.1 and TA1.2 TACCRx value such that its PWM is 50%.  

Consider what additional hardware you'll need (regulator, motor driver chip, decoupling capacitor, etc.) and how you'll configure / connect it.

- Initially I will need to use a motor driver chip in order to get the required voltage to power the motors. This can be seen in the schematic.

Consider the interface you'll want to create to your motors. Do you want to move each motor invidiually (moveLeftMotorForward())? Or do you want to move them together (moveRobotForward())?

-I want to move the robots motors together. I will have functions to move the robot forward, backward, CW, and CCW.


### Hardware schematic
Below is the schematic of the breadboard, MSP430, and motors and how they will be connected and supplied power.

![](images/schematic.JPG)

### Flowchart 



![](images/Flowchart.JPG)
### Debugging
One particular issue I had when trying to get required functionality to work was that I was not getting one of my wheels to move at all. In order to debug it, I copied the exact code that was given to us in the lesson notes and tried to capture the PWM on an O-scope however was getting a continuous voltage. Determined that I had somehow destroyed my MSP430. I got a new one and plugged in into my board and the robot worked as expected.

### Testing methodology or results

##### Functionality
The required functionality for this lab was to execute movement forward, backward, a small (< 45 degree) turn left and right, and a large (> 45 dgree) turn left and right sequentially completely disconnected from a computer.  A functionality for this lab was to control the robot using the remote that we used during lab 5.  It used 4 buttons to have movement forward, backward, left and right.  Below can be found the video that demonstrates A functionality using the remote. (Cap. Falkinburg said that a video for A functionality alone would suffice for both functionalities).

[Required Functionality](https://www.youtube.com/watch?v=OPi13y56NGA)


### Observations and Conclusions
The objective of this lab was to use to use PWMs in order to control the speed and direction of the wheels of the robot to control its movement. In addition, we were to connect our remote that used IR interrupts from the last lab to control the robot's movement. Because I was able to get the robot to move in all directions that were desired without and with the remote, I successfully met the objective of this lab. I will use the skills learned from this lab in order to allow the robot to eventually maneuver through a maze using the servo and ultrasonic sensor in future labs.

### Documentation
None.