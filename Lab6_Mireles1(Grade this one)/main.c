/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: 17 Nov 16
Course: ECE 382
File: main.c
Event: Lab 6

Purp: Measure IR Pulses and calls the function to move the robot

Doc: Code for IR from Lab 5.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h>
#include "Lab6.h"
//-----------------------------------------------------------------------------------------------
// does this work?
//-----------------------------------------------------------------------------------------------
int8	newIrPacket = FALSE;
int16	packetData[48];
int8	packetIndex = 0;
int32	irPacket = 0;
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
char ultrasonic_flag = 0;
int main(void) {
	initMSP430();
	while(1) {
//				servo();
				if (packetIndex > 33) {
					packetIndex = 0;
				}
//				__delay_cycles(80000);
				if(irPacket==UP){
					forward();
				}
				else if(irPacket==DOWN){
					backward();
				}
				else if(irPacket==RIGHT){
					right90();
				}
				else if(irPacket==LEFT){
					left90();
				}

			}
}


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file
__interrupt void pinChange (void) {

	int8	pin;
	int16	pulseDuration;						// The timer is 16-bits

	if (IR_PIN)		pin=1;	else pin=0;

	switch (pin) {								// read the current pin level
		case 0:									// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TA0R;				//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R
			if(TA0R >= minLogic1Pulse && TA0R <= maxLogic1Pulse){
				irPacket <<= 1;
				irPacket |= 0x0001;
			}
			else{
				irPacket <<= 1;
				irPacket &= ~0x0001;
			}
			packetData[packetIndex++] = pulseDuration;
			TA0CTL &= ~MC_1;
			LOW_2_HIGH; 						// Set up pin interrupt on positive edge
			break;

		case 1:									// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TA0R = 0x0000;						// time measurements are based at time 0
			TA0CTL |= MC_1|TAIE;				// Turn on timer A, enable timer A interrupt
			HIGH_2_LOW; 						// Set up pin interrupt on falling edge
			break;
	} // end switch

	P2IFG &= ~BIT6;			// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {
	TA0CTL &= ~(MC_1|TAIE);					// Turn off timer A, Turn off timer A interrupt
	packetIndex = 0;						// Clear packet index
	newIrPacket = TRUE;						// set newPacket flag
	TA0CTL &= ~TAIFG;						// Clear TAIFG

}

