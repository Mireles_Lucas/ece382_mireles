/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: October 12, 2016
Course: ECE 382
File: move.c
Event: Assignment 8 - Moving Average

Purp: Where the header functions are actually stored and complete the desired task.

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include "move.h"

// Moving average functions
int getAverage(int array[], unsigned int arrayLength){ //gets the average of an array
	int sum = 0;
	int i;
	for (i = 0; i < arrayLength ; i ++){
		sum += array[i];
	}
	return sum/arrayLength;
}

void addSample(int sample, int array[], unsigned int arrayLength){//adds a value to the looked at sample array
	int i = 0;
	for (i = 0; i < arrayLength - 1; i++){
		array[i] = array[i + 1];
	}
	array[i] = sample;
	return;
}

void addValue( int value, int array[], int position){//adds a value to a given array
	array[position] = value;
	return;
}

// Array functions
int max(int array[], unsigned int arrayLength){//finds the max value of a given array
	int max = 0;
	int i;
	for (i = 0; i < arrayLength; i++){
		if (array[i] > max){
			max = array[i];
		}
	}
	return max;
}

int min(int array[], unsigned int arrayLength){//finds the min value of a given array
	int min = array[0];
	int i;
	for (i = 0; i < arrayLength; i++){
		if (array[i] < min){
			min = array[i];
		}
	}
	return min;
}

unsigned int range(int array[], unsigned int arrayLength){//finds the range of a given array
	return max(array, arrayLength) - min(array, arrayLength);
}
