/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: October 12, 2016
Course: ECE 382
File: main.c
Event: Assignment 8 - Moving Average

Purp: Where the max, min, an range functions are called and where averages array is updated.

Doc:    C2C Hanson told me that I need to put volatile before my unused variables so they show up

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430.h> 
#include "move.h"
/*
 * main.c
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	int sample[N_AVG_SAMPLES];
	int average_2b_added;
	volatile int max_value;
	volatile int min_value;
	volatile int array_range;
	int counter;
	int i;
	int first_test[10]={174, 162, 149, 85, 130, 149, 153, 164, 169, 173};
	int first_test_avg[11];
	for (i = 0; i < 11; i++){
			first_test_avg[i] = 0;
		}
	for (i = 0; i < N_AVG_SAMPLES; i++){
		sample[i] = 0;
	}
	for (counter = 0; counter < 11; counter++){
		average_2b_added = getAverage(sample, N_AVG_SAMPLES);
		addValue( average_2b_added, first_test_avg, counter);
		addSample(first_test[counter], sample, N_AVG_SAMPLES);
	}
	max_value = max(first_test, 10);
	min_value = min(first_test, 10);
	array_range = range(first_test, 10);

	while(1);
}
