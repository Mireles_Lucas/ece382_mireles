#  Assignment 8 - Moving Averages

## By Lucas Mireles 

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Debugging](#debugging)
3. [Testing methodology or results](#testing-methodology-or-results)
4. [Documentation](#documentation)
 
### Objectives or Purpose 
- Write a function that computes the "moving average" for a stream of data points it is receiving.
    - A moving average computes the average of the most recent `N_AVG_SAMPLES` points.
    - Initialize values in the samples array to be 0.
    - For example: (`N_AVG_SAMPLES` = 2), sample stream = (2, 4, 6, 8).
        - Starting point: average [0, 0]
            - Average is 0
        - First sample: average [0, 2]
            - Average is 1
        - Second sample: average [2, 4]
            - Average is 3
        - Third sample: average [4, 6]
            - Average is 5
        - Fourth sample: average [6, 8]
            - Average is 7
    - Use the following data streams to test your moving average function:
        - Test these streams with (`N_AVG_SAMPLES` = 2) and (`N_AVG_SAMPLES` = 4).
        - (45, 42, 41, 40, 43, 45, 46, 47, 49, 45)
        - (174, 162, 149, 85, 130, 149, 153, 164, 169, 173)
        - Note: you can store the "moving average" values into an array to make your simulation and verification stages simpler.
        - Additional Note:
            - You should treat the stream values as coming in one at a time
            - You don't know the value of future samples in advance!
            - That means your implementation shouldn't be dependent on passing in an array of samples!
            - Look at [my header file](moving_average_h.html) if you're having trouble visualizing an interface
- Write two other functions that compute the maximum and minimum values in a given array.  You can "terminate" (last element is some predefined constant to simplify your loop) or pass in the length of the array as a parameter.
- Write another function that computes the range of values in an array.

### Debugging 
The biggest problem I had was that in my max and min functions, instead of setting my max or min value to what the certain value in the array was, I was setting the array value to whatever the max or min value was initialized to which continued to give me the wrong answer.

###Testing and Answers to Lab Questions 
Below are screen shots that I took of my variables after running all of my functions for each test cases and for both `N_AVG_SAMPLES` = 2 and `N_AVG_SAMPLES` = 4. They show both the average array, the actual test case, and the max, min, and range values. The name of the variable or array is in the left column and the actual value of the variable is in the right column.
#######The first test case was (45, 42, 41, 40, 43, 45, 46, 47, 49, 45).
![](images/first_test_2.JPG)
###### First Test Stream - `N_AVG_SAMPLES` = 2

![](images/first_test_4.JPG)
###### First Test Stream - `N_AVG_SAMPLES` = 4 

#######The second test case was (174, 162, 149, 85, 130, 149, 153, 164, 169, 173).

![](images/second_test_2.JPG) 
###### Second Test Stream - `N_AVG_SAMPLES` = 2
 
![](images/second_test_4.JPG)
###### Second Test Stream - `N_AVG_SAMPLES` = 4


###Questions 
1. I used integers as my array types because I wanted whole numbers not floats.
2. I calculated the min, max, and range of the given test cases by hand then compared the results of what my functions produced. For the moving averages array, I hand calculated the average for each sample then compared my array to my hand results.
### Documentation
None
