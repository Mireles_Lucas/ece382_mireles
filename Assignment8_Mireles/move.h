/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: October 12, 2016
Course: ECE 382
File: move.h
Event: Assignment 8 - Moving Average

Purp: Functions to update and monitor a moving average

Doc: None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef _MOV_AVG_H
#define _MOV_AVG_H
#define N_AVG_SAMPLES 4

// Moving average functions
int getAverage(int array[], unsigned int arrayLength); //Gets the average of a passed array
void addSample(int sample, int array[], unsigned int arrayLength);//adds a value to the looked at sample array
void addValue(int value, int array[], int position);//adds a value to an array

// Array functions
int max(int array[], unsigned int arrayLength); //finds the max value of an array
int min(int array[], unsigned int arrayLength);//finds the min value of an array
unsigned int range(int array[], unsigned int arrayLength);// finds the range of an array

#endif
