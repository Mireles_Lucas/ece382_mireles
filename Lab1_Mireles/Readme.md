#  Lab 1 - A Simple Calculator

## By Lucas Mireles 

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Assembly Directives](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Observations and Conclusions](#observations-and-conclusions)
8. [Documentation](#documentation)
 
### Objectives or Purpose 
The purpose of this lab is to create a simple calculator, one that can do addition, subtraction, and multiplication, in assembly language.

My program will start reading at the location in ROM where I stored the calculator instructions. It will read the first byte as the first operand. The next byte will be an operation. There are 4 total 5 total operations: addition, subtraction, multiplication, clear, and end. The third byte will be the second operand. My program will execute the expected operation and store the result starting at 0x0200. The result will then be the first operand for the next operation. The next byte will be an operation. The following will be the second operand. My program will execute the requested operation and store the result at 0x0201. My program will continue doing this until you encounter an END_OP - at which point, your program will cease execution.

### Preliminary design
The first step in designing the calculator is to store the bytes that my calculator will be reading. This can be seen in the Assembly Directives section as myProgram. This stores the operands and operations that will be performed.  The step is to allocate space for my results to be stored in. This is done under the Assembly Directives section as myResults. The last step in the Assembly directives section is to use equate statements to store the operation codes as constants which can as well be seen below under Assembly Directives.

######Assembly Directives:
	;-------------------------------------------------------------------------------
	myProgram:	  .byte	0x05, 0x33, 0x02, 0x22, 0x11, 0x33, 0xFF, 0x11, 0xFE, 0x33, 0x01, 0x33, 0x02, 0x22, 0xFE, 0x55 ; Test case 2
	ADD_OP: 	  .equ	0x11
	SUB_OP: 	  .equ	0x22
	MUL_OP:  	  .equ	0x33
	CLR_OP: 	  .equ	0x44
	END_OP: 	  .equ	0x55
	clear_result: .equ  0x00
	max_result:   .equ  0xFF
	              .data
	myResults:    .space      20              	; reserving 20 bytes of space		
	;-------------------------------------------------------------------------------
	
Now that background steps are done, the actual implementation of the calculator can begin. As seen on the flow chart below, the first step is to read the first operand from memory. The next step is to read the operation from memory. If the operation is End, then the program will end using a CPU trap. If not, it will continue and read the second operand. If the operation is Clear, then it will set the result as 0x00, store the result in memory, then use the 2nd operand as the first operand for the next operation. If the operation is not Clear, then the calculator will do the math operation, then store the result in memory. If the number is greater than 255 it will store it as 255 and if it less than 0, it will store it as 0. The result will then be used as the first operand for the next operation. 

### Software flow chart 
![Flow Chart](images/FlowChart.JPG)
##### Figure 1: Flow Chart of Simple Calculator 


### Debugging
The problem that I encountered involved testing whether or not my result was greater than 255 or less than 0. Because I am inputting and outputting unsigned bytes, I needed to make sure that I was testing for an unsigned byte. After some debugging, I realized that I need to the command JGE and JL and instead of JHS and JLO. 

### Testing methodology or results
The first test that I completed involved the required functionality of the calculator. The required functionality ensured that the addition, subtraction, clear, and end operations were fully functional and that each result was stored in memory correctly.The test case used to test the functionality was: 0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55.  Based off of these inputs, the following are the expected results: 0x22, 0x33, 0x00, 0x00, 0xCC. By looking at Figure 2, both the expected and actual results match concluding that the required functionality is met.

![Required Functionality Results](images/required.JPG)
##### Figure 2: Required Functionality Test Case Results

The next test tested the B functionality of the calculator. This requirement was for any result greater than 255 to be set to and stored into memory as 255 and any result less than 0 to be set to and stored as 0. The test case used to test the functionality was 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55.  Based off of these inputs, the following are the expected results: 0x22, 0x33, 0x44, 0xFF, 0x00, 0x00, 0x00, 0x02. By looking at Figure 3, both the expected and actual results match concluding that the B functionality is met.

![Required Functionality Results](images/Bfunctionaity.JPG)
##### Figure 3: B Functionality Test Case Results

The final case given to me to test was A functionality which added the multiplication operation to the calculator. The test case used to test the multiplication functionality was: 0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55.  Based off of these inputs, the following are the expected results: 0x44, 0x11, 0x88, 0x00, 0x00, 0x00, 0xff, 0x00, 0xff, 0x00, 0x00, 0xff. By looking at Figure 4, both the expected and actual results match concluding that the multiplication operation works.


![Required Functionality Results](images/Afunctionality.JPG)
##### Figure 4: A Functionality Test Case Results

The next test case that I conducted was one that I created on my own. This particular test case would focus on the required and B functionality of the calculator: 0xFE, 0x11, 0x01, 0x11, 0x01, 0x22, 0x01, 0x11, 0x03, 0x22, 0xFF, 0x22, 0x03, 0x22, 0x01, 0x11, 0x02, 0x55. I chose this test case because it tested the extremes of the maximum and minimum numbers that could be stored. THis test involved adding numbers to an already maxed out number of 255 and vice versa, subtracting numbers that were already at 0 to ensure that my calculator would not break given these conditions. The expected results are: 0xFF, 0xFF, 0xFE, 0xFF, 0x00, 0x00, 0x00, 0x02. By looking at Figure 5, both the expected and actual results match concluding that my first test case is met. 


![Required Functionality Results](images/FirstTestCase.JPG)
##### Figure 5: First Test Case  Results

The final test case that I conducted focused on the multiplication operation combined with the b functionality. In other words, I wanted to test how the multiplication operation would work if the result exceeded 255 or if the result was supposed to be negative (10-11=-1) then multiply by that number. The following is the test case used: 0x05, 0x33, 0x02, 0x22, 0x11, 0x33, 0xFF, 0x11, 0xFE, 0x33, 0x01, 0x33, 0x02, 0x22, 0xFE, 0x55. The expected results are: 0x0A, 0x00, 0x00, 0xFE, 0xFE, 0xFF, 0x01. By looking at Figure 6, both the expected and actual results match concluding that my second test case is met. 

![Required Functionality Results](images/SecondTestCase.JPG)
##### Figure 6: Second Test Case Results

### Observations and Conclusions
The purpose of this lab is to create a simple calculator, one that can do addition, subtraction, and multiplication, in assembly language. At the conclusion of this lab, I believe that I have met this purpose and that I have created a calculator that can successfully do these simple math operations. In addition to this, the most noteworthy lesson that I learned from this lab was Assembly Directives. Previously, I had no idea how to really use them. However now, I am comfortable using .equ statement to hold constant values, allocate space to store results using the .space directive, and using the .byte directive to store the inputs being used by the program in memory. Because of this, I will now be able to implement Assembly directives on the rest of my labs.

### Documentation
None
