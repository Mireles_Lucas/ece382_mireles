;-------------------------------------------------------------
; Lab1_Mireles - A Simple Calculator
; Capt Jeff Falkinburg, USAF / 12 Sept 2016
;
; This program is a designing a simple calculator using assembly code
;
;-------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
;myProgram:  .byte   0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55 ; required
;myProgram:  .byte   0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55 ;B functionality
;myProgram:  .byte	0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55 ; A functionality
;myProgram:  .byte   0xFE, 0x11, 0x01, 0x11, 0x01, 0x22, 0x01, 0x11, 0x03, 0x22, 0xFF, 0x22, 0x03, 0x22, 0x01, 0x11, 0x02, 0x55 ; First test case (regards B funtionaity and required)
; expected   0xFF, 0xFF, 0xFE, 0xFF, 0x00, 0x00, 0x00, 0x02
myProgram:	.byte	0x05, 0x33, 0x02, 0x22, 0x11, 0x33, 0xFF, 0x11, 0xFE, 0x33, 0x01, 0x33, 0x02, 0x22, 0xFE, 0x55 ; Test case 2
; expected   0x0A, 0x00, 0x00, 0xFE, 0xFE, 0xFF, 0x01
ADD_OP: 	.equ	0x11
SUB_OP: 	.equ	0x22
MUL_OP:  	.equ	0x33
CLR_OP: 	.equ	0x44
END_OP: 	.equ	0x55
clear_result: .equ  0x00
max_result: .equ    0xFF

            .data
myResults:  .space      20              	; reserving 20 bytes of space
            .text


;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
			mov #myProgram, r5
			mov #myResults, r7
			mov.w #max_result, r11
			mov.b @r5+, r6		;first operand
start:
			mov #clear_result, r12 ;makes r12 equal to 0.
			mov.b @r5+, r8		;operation

			cmp #END_OP, r8		;compares to see if the operation is a end
			jz forever

			mov.b @r5+, r9		;second operand

			cmp #ADD_OP, r8		;jumps if add op
			jz Addition

			cmp #SUB_OP, r8		;jumps if sub op
			jz Subtraction

			cmp #CLR_OP, r8		;jumps if clr op
			jz Clear

			cmp #MUL_OP, r8
			jz Multiply 		;jumps if mul op

Addition:
			add.w r9, r6		; adds the second operand to the first
			jmp store

Subtraction:
			sub.w r9, r6		; subtracts the second operand from the first
			jmp store

Multiply:
			cmp #0, r6			; checks to see if any of the operands are equal to 0, is so the result is set to 0 and is stored
			jz less
			cmp #0, r9
			jz less
			mov r6, r12
Not_zero:
			dec r9				; multiplies using a loop and decrmenting one of the operands until is either 0 (if the second operand is a 1 to start) or 1
			cmp #1, r9			; if it is not 1 to start
			jl store
			add r12, r6
			cmp #1, r9
			jz store
			jmp Not_zero

Clear:
			mov.b r9, r6		; stores 0 into memory then makes second operand the first
			mov.b #clear_result, 0(r7)
			inc r7
			jmp start

store:
			cmp.w r11, r6		; checks to see if the result is greater than 255
			jge greater
			cmp.w r12, r6		; checks to see if the result is less than 0
			jl less
			mov.b r6, 0(r7)
			inc r7
			jmp start
greater:						; sets the result to 255 if it is greater than 255
			mov.b #max_result, r6
			mov.b r6, 0(r7)
			inc r7
			jmp start
less:
			mov.b #clear_result, r6
			mov.b r6, 0(r7)		; sets the result to 0 if it is less than 0
			inc r7
			jmp start


forever:
			jmp forever			;CPU trap

                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
