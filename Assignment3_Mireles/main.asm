;-------------------------------------------------------------
; Assignment3_Mireles - Control Flow
; Capt Jeff Falkinburg, USAF / 31 Aug 2016
;
; This program reads a number from memory then decides what to do with it using conditional loops
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
			mov #0x0801, &0x0216 ;stores word into memory
			mov #0, r9
			mov #20, r8     ; moves 20 into register 8
			mov #1, r10
			mov &0x0216, r6 ;sets register to the number stored in memory
			cmp #0x1234, r6 ;compares the entered number with 0x1234
			jeq equalorless
			jlo equalorless  ;jumps if it is equal or less than
greater:
			add r8, r9		;adds two registers and stores in r9
			mov r9, &0x0206 ;stores the value of r9 into memory
			dec r8 			; decrements the valu of r8 by 1
			jz forever		; if r8 = 0 then CPU trap
			jmp greater		; if not, loop until = 0

equalorless:
			cmp #0x1000, r6 ;compares the entered number with 0x1000
			jz divormul		;
			jlo divormul		;jumps if it is equal to or less than
			add #0xEEC0, r6 ; if it is greater than, it adds 0xEEC0 to that number then stores in memory
			mov.b r6,&0x0202
			jmp forever		; jumps to CPU trap
divormul:
			and r6, r10		; ands the value being compared with 1 to see if it is even or not
			jz even			; if the zero bit is set then it is an even number so jump to even
			rla r6 			; it is an odd number so multiply by 2 then store into memory
			mov.w r6, &0x0212
			jmp forever
even:
			rra r6			; divdes by 2 then stores in memory
			mov.w r6, &0x0212
forever:					;CPU trap
			jmp forever

                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
