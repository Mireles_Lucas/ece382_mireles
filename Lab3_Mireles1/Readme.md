#  Lab 3 - SPI - "I/O"

## By Lucas Mireles 

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Subroutines](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Observations and Conclusions](#observations-and-conclusions)
8. [Documentation](#documentation)
 
### Objectives or Purpose 
The purpose of this lab is to write a program that allows inputs to be made that create a 10x10 pixel box to appear on an LCD screen at the push of a button and to be able to move that 10x10 box using inputs from buttons.

### Preliminary design
The first step designing this program was to complete the Mega Prelab. The prelab allowed me to become familiar with how the MSP430 interacted with the LCD board that I will be writing my program to through the use of tracing pins to ports, creating timing diagrams, and even creating a delay routine. The delay routine can be found below along with the calculations needed to ensure that the delay was 160ms.

###### My Subroutines (Delay160ms and DrawBox):
	;----------------------------------------------------------------------------;-------------------------------------------------------------------------------
	;	Name: Delay160ms
	;	Inputs: none
	;	Outputs: none
	;	Purpose: creates ~160ms delay
	;	Registers: r7 preserved
	;-------------------------------------------------------------------------------
	Delay160ms: ; 5 cycles
                     push	r8						; 3cycles
                     mov.w	#10, r8 				; 2 cycles
                     push   r7						; 3 cycles
	decrement_delay:

                     mov.w  #0xC47F, r7 			; 2 cycles

	delay:               dec           r7               ; 1 cycle
                     jnz           delay            ; 2 cycles
                     dec		   r8				; 1 cycle
                     jnz		   decrement_delay  ; 2 cycles
                     pop 		   r8				; 2 cycles
                     pop           r7               ; 2 cycles
                     ret                            ; 3 cycles



	; 13 + 2*10 + (0xc47f*3+3)10 + 7 = 148148 cycles (calculated using a 106ns)
	;-------------------------------------------------------------------------------
	;	Name: DrawBox
	;	Inputs: xStart in r12, yStart in r13,
	;	Outputs: none
	;	Purpose: creates a 10x10 box
	;	Registers: all registers preserved
	;-------------------------------------------------------------------------------
	DrawBox:
					push r10
					push r12
					push r13
					push r14
					push r15

					mov #10, r10 ; loop counter to draw 10 lines
					mov #XLINELEN, r14
					add r12, r14; setting the x end to 10 pixels from the start
					mov r13, r15 ; sets the y end and start to the same
	DoAgain:
					call #drawLine
					inc r13	; incremens the y axis for both start and end
					inc r15
					dec r10
					jnz DoAgain

					pop r15
					pop r14
					pop r13
					pop r12
					pop r10

					ret
		
	;------------------------------------------------------------------------------- 
To ensure that the delay routine met the required time, I first needed to measure how long my clock actually was. Once this was measured using a logic analyzer, I then measured how long the delay actually took using a logic analyzer as well. The results can be seen below.
![clock length](images/length.jpg)
##### Length of the Cloc (106ns)
![clock length](images/Delay.jpg)
##### Length of my delay (160.748ms)

Additionally, the prelab required that I complete a bit logic diagram. Below is the results of the bit logic diagram.
![bitlogic](images/BitLogic.jpg)
##### Bit Logic Diagram 
After my delay subroutine was complete, it was time to create a flowchart that outlines what I will be accomplishing in the program. Because I was given many subroutines such as SetArea, WriteData, WriteCommand, and Drawline, my flowchart focuses on my main and the subroutine that I created: DrawBox. The flowchart can be found below while my subroutine DrawBox can be found above.
![flowchart](images/FlowChart.JPG)
##### Flowchart
The next step in the lab was to ensure that I understood how the MSP430 was communicating with the LCD. To ensure I knew how this was working, we were to use a logic analyzer to capture several packets of data that were being sent to the LCD, causing a horizontal bar to be drawn. I found three calls to writeCommand and eight calls to writeData that generate these packets being sent from the SetArea subroutine of the given code. Because our MSP430's phase is 1 and polarity is 0, the data is captured from the MOSI on serial clock's first edge (rising edge). The results are seen below.
### Logic Analyzer  
![Packet1](images/Packet1.jpg)
##### Packet 1 - 0x2A - Sets Column Address (Write Command)
![Packet1](images/Packet2.jpg)
##### Packet 2 - 0x00 - MSB of start column (SC)
![Packet1](images/Packet3.jpg)
##### Packet 3 - 0x0D - LSB of SC
![Packet1](images/Packet4.jpg)
##### Packet 4 - 0x00 - MSB of end column (EC)
![Packet1](images/Packet5.jpg)
##### Packet 5 - 0x12 - LSB of EC
![Packet1](images/Packet6.jpg)
##### Packet 6 - 0x2B - Set Page Address (Write Command)
![Packet1](images/Packet7.jpg)
##### Packet 7 - 0x00 - MSB of Start Page(SP)
![Packet1](images/Packet8.jpg)
##### Packet 8 - 0x07 - LSB of SP
![Packet1](images/Packet9.jpg)
##### Packet 9 - 0x00 - MSB of End Page (EP)
![Packet1](images/Packet10.jpg)
##### Packet 10 - 0x07 - LSB of EP
![Packet1](images/Packet11.jpg)
##### Packet 11 - 0x2C - Memory Write (Write Command)
As seen above, their are 11 packets sent. The first packet is a command that says I am about to set the column address. Packets 2-5 are data packets that tell the program where the start and end of the column is going to be. Packet 6 tells the program that I am going to set the Page address. Packets 7-10 tell the program where the start and end of the page is going to be. Finally, Packet 11 actually writes this to memory. The above information was compressed into a table below that shows the results of the logic analyzer.
![Table](images/LogicAnalyzer.JPG)
##### Logic Analyzer - 8-bit Waveforms after S1 is pressed

### Debugging
My biggest problem in the lab was that I continued to create a 9x10 box instead of a 10x10 box. This was due to me not calling the DrawLine subroutine in my DrawBox function enough times. After Capt. Falkinburg pointed out this issue, I simply added 1 to my loop counter in my DrawBox subroutine.

### Testing methodology or results
After creating the DrawBox subroutine, it was time to test the functionality of the box. After loading the program onto my LCD and having a clear screen, I pushed S1 and the following box appeared on my LCD.

![Required Functionality Results](images/result1.jpg)
##### Required Functionality 

I know that it is correct because it begins in the top right corner at the specified starting point which can found in the main.asm and is 10x10.

Once the required functionality is complete, it was time to complete A functionality by allowing the user to move the block by 10 pixels upon the press of a button. The only changes I needed to make to my program was to check for different button presses and update the x and y coordinates accordingly. After pressing the right button (S5) 3 times and the down button (S3) three times, the following image appeared.

![Required Functionality Results](images/result2.jpg)
##### A Functionality Test Case Results

This image as well concludes that my program works correctly because the starting x and y coordinates of the resulting box is 30 pixels to the right and 30 pixels down.

### Observations and Conclusions
The purpose of this lab was to create a program that made a 10x10 box appear on the LCD and allowed the user to control the location of the box based upon his or her inputs. I have completed this objective and successfully met the requirements for both required functionality and A functionality.

Additionally, the most noteworthy knowledge I have gained through this lab was how to successfully understand how the MSP430 communicates with the LCD. I now understand how the different ports and pins interact and how to initialize certain pins to do certain functions. I will now be able to use this knowledge to configure the MSP430 in future use.

### Documentation
None
