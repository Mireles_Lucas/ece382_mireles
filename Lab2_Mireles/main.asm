;-------------------------------------------------------------
; Lab2_Mireles - Cryptology
; Capt Jeff Falkinburg, USAF / 12 Sept 2016
;
; This program is a designing a simple calculator using assembly code
;
;-------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            		.text                           ; Assemble into program memory
            		.retain                         ; Override ELF conditional linking
encrypted_text: 	.byte  0xf8,0xb7,0x46,0x8c,0xb2,0x46,0xdf,0xac,0x42,0xcb,0xba,0x03,0xc7,0xba,0x5a,0x8c,0xb3,0x46,0xc2,0xb8,0x57,0xc4,0xff,0x4a,0xdf,0xff,0x12,0x9a,0xff,0x41,0xc5,0xab,0x50,0x82,0xff,0x03,0xe5,0xab,0x03,0xc3,0xb1,0x4f,0xd5,0xff,0x40,0xc3,0xb1,0x57,0xcd,0xb6,0x4d,0xdf,0xff,0x4f,0xc9,0xab,0x57,0xc9,0xad,0x50,0x80,0xff,0x53,0xc9,0xad,0x4a,0xc3,0xbb,0x50,0x80,0xff,0x42,0xc2,0xbb,0x03,0xdf,0xaf,0x42,0xcf,0xba,0x50
length: 			.equ 81
key:				.byte 0xac, 0xdf, 0x23
key_length: 		.equ 0x03
            		.data
decrypted_message:  .space 100
					.text
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;----------------------------------------------------------------------------

            ;
            ; load registers with necessary info for decryptMessage here
            ;
            mov		#key, r6
			mov 	#encrypted_text, r5
			mov 	#decrypted_message, r7
			mov 	#length,r8
			mov 	#key_length, r11
			mov		#0, r12 		; so i can use r12 a key counter
			mov		r6, r13			; making a copy

main:
            call    #decryptMessage


forever:
			jmp     forever

;-------------------------------------------------------------------------------
                                            ; Subroutines
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;Subroutine Name: decryptMessage
;Author:
;Function: Decrypts a string of bytes and stores the result in memory.  Accepts
;           the address of the encrypted message, address of the key, and address
;           of the decrypted message (pass-by-reference).  Accepts the length of
;           the message by value.  Uses the decryptCharacter subroutine to decrypt
;           each byte of the message.  Stores theresults to the decrypted message
;           location
;Inputs:
;Outputs:
;Registers destroyed:
;-------------------------------------------------------------------------------

decryptMessage:

			mov.b 	@r13+, r9	;putting the copy of the key into here, just the first byte then increments
			mov.b	@r5+, r10	; puts one byte of the  encrypted message into r10
			call 	#decryptCharacter
			mov.b 	r10, 0(r7)	; puts the decrypted message into memory.
			inc		r12	;increments the key counter to see if i need to start the key over or keep going.
			cmp 	#key_length, r12 ; compares the key counter with length of key
			jnz		continue
			mov		r6, r13	; if they equal, resetting the key to the first byte
			mov 	#0, r12
continue:
			inc 	r7	; increments the space where i will putting memory into then decrements the length of the message to see if im done or not.
			dec 	r8
			cmp 	#0, r8
			jnz 	decryptMessage


            ret

;-------------------------------------------------------------------------------
;Subroutine Name: decryptCharacter
;Author:
;Function: Decrypts a byte of data by XORing it with a key byte.  Returns the
;           decrypted byte in the same register the encrypted byte was passed in.
;           Expects both the encrypted data and key to be passed by value.
;Inputs:
;Outputs:
;Registers destroyed:
;-------------------------------------------------------------------------------

decryptCharacter:

			xor 	r9, r10	;xor with the byte of the key to get the decrypted message
			;call 	#check
            ret

;check:					; subroutine for A functionality however I could not complete it.
;           cmp     #0x41, r10
;            jl        check_spaceorperiod
;            cmp     #0x5B, r10
;            jge        check_upper
;            jmp     continue_check

;check_upper:
;            cmp     #0x61, r10
;            jl        increment_key
;            cmp     #0x7B, r10
;            jge     increment_key
;            jmp     continue_check

;check_spaceorperiod:
;            cmp     #0x20, r10
;            jz        continue
;            cmp     #0x2E, r10
;            jz         continue_check
;            jmp     increment_key

;increment_key:
;            inc r6
;            jmp start
;continue_check:
;            ret
;-------------------------------------------------------------------------------


;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect    .stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
