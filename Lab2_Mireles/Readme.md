#  Lab 2 - Cryptography

## By Lucas Mireles 

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Assembly Directives](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Observations and Conclusions](#observations-and-conclusions)
8. [Documentation](#documentation)
 
### Objectives or Purpose 
The purpose of this lab is to write a program that decrypts an encrypted message using a simple encryption technique by writing subroutines. In addition, I will use both the call-by-value and call-by-reference techniques to pass arguments to the subroutines.

### Preliminary design
The first step in decrypting the message is to store the encrypted message in ROM. In addition to the encrypted message, I need to store the length of the message, the key for decrypting the message and the length of the key in ROM as well.  This can be seen below in the Assembly Directives section. I as well need to allocate space for decrypted message to be stored in RAM which can also be seen below in the Assembly Directives section. After these have been completed, we can look at my flowchart to understand the basic idea behind this decryption program.

######Assembly Directives:
	;-------------------------------------------------------------------------------
	            		.text                           ; Assemble into program memory
            			.retain                         ; Override ELF conditional linking
	encrypted_text: 	.byte  0xf8,0xb7,0x46,0x8c,0xb2,0x46,0xdf,0xac,0x42,0xcb,0xba,0x03,0xc7,0xba,0x5a,0x8c,0xb3,0x46,0xc2,0xb8,0x57,0xc4,0xff,0x4a,0xdf,0xff,0x12,0x9a,0xff,0x41,0xc5,0xab,0x50,0x82,0xff,0x03,0xe5,0xab,0x03,0xc3,0xb1,0x4f,0xd5,0xff,0x40,0xc3,0xb1,0x57,0xcd,0xb6,0x4d,0xdf,0xff,0x4f,0xc9,0xab,0x57,0xc9,0xad,0x50,0x80,0xff,0x53,0xc9,0xad,0x4a,0xc3,0xbb,0x50,0x80,0xff,0x42,0xc2,0xbb,0x03,0xdf,0xaf,0x42,0xcf,0xba,0x50
	length: 			.equ 81
	key:				.byte 0xac, 0xdf, 0x23
	key_length: 		.equ 0x03
            			.data
	decrypted_message:  .space 100
						.text		
	;------------------------------------------------------------------------------- 

### Software flow chart 
![Flow Chart](images/flowchart.JPG)
##### Figure 1: Flow Chart of Simple Calculator 
The program is fairly simple.  Once I have stored all the values needed into registers, we begin the program by calling the subroutine decryptmessage. This subroutine then calls decryptcharacter which XORs the encrypted message with the correct key. It then returns to decryptmessage which stores the decrypted message in memory. This process continues until every byte of the message has been decrypted.

### Debugging
My biggest problem in the lab was getting the A functionality to work, which I ended up not being able to do.  I was able to find out that the first byte of the key was 0x73 by brute force of going through my memory and seeing if I was getting any sort of message. Once I determined the first byte of the key, I saw that every other byte in memory was a character however I was obviously missing the other part so only had half the message. In order to determine the total key, I attempted to begin my key at 0x0000 then increment form there until I had the correct value. I would increment the key if I were to get an invalid ascii value that was decrypted by checking the newly decrypted message's hex value to a range of ascii hex values and seeing if they were valid. If they were, I would keep that key until I got an invalid message then I would increment the key and start the program over. I called this subroutine "check" and was called in decrpytcharacter. My attempt can be found in main.asm.

### Testing methodology or results
The first test that I completed involved the required functionality of the program. I was given the encrypted message: 0xef,0xc3,0xc2,0xcb,0xde,0xcd,0xd8,0xd9,0xc0,0xcd,0xd8,0xc5,0xc3,0xc2,0xdf,0x8d,0x8c,0x8c,0xf5,0xc3,0xd9,0x8c,0xc8,0xc9,0xcf,0xde,0xd5,0xdc,0xd8,0xc9,0xc8,0x8c,0xd8,0xc4,0xc9,0x8c,0xe9,0xef,0xe9,0x9f,0x94,0x9e,0x8c,0xc4,0xc5,0xc8,0xc8,0xc9,0xc2,0x8c,0xc1,0xc9,0xdf,0xdf,0xcd,0xcb,0xc9,0x8c,0xcd,0xc2,0xc8,0x8c,0xcd,0xcf,0xc4,0xc5,0xc9,0xda,0xc9,0xc8,0x8c,0xde,0xc9,0xdd,0xd9,0xc5,0xde,0xc9,0xc8,0x8c,0xca,0xd9,0xc2,0xcf,0xd8,0xc5,0xc3,0xc2,0xcd,0xc0,0xc5,0xd8,0xd5,0x8f. The key for this message was 0xac. After running the program, I received the following message in my memory browser below in Figure 2 concluding that the required functionality was met.

![Required Functionality Results](images/Required_functionality.JPG)
##### Figure 2: Required Functionality Test Case Results

The next test tested the B functionality of my program. I was given the encrypted message: 0xf8,0xb7,0x46,0x8c,0xb2,0x46,0xdf,0xac,0x42,0xcb,0xba,0x03,0xc7,0xba,0x5a,0x8c,0xb3,0x46,0xc2,0xb8,0x57,0xc4,0xff,0x4a,0xdf,0xff,0x12,0x9a,0xff,0x41,0xc5,0xab,0x50,0x82,0xff,0x03,0xe5,0xab,0x03,0xc3,0xb1,0x4f,0xd5,0xff,0x40,0xc3,0xb1,0x57,0xcd,0xb6,0x4d,0xdf,0xff,0x4f,0xc9,0xab,0x57,0xc9,0xad,0x50,0x80,0xff,0x53,0xc9,0xad,0x4a,0xc3,0xbb,0x50,0x80,0xff,0x42,0xc2,0xbb,0x03,0xdf,0xaf,0x42,0xcf,0xba,0x50. The key for the B Functionality was 0xacdf23. After running the program, I received the following message in my memory browser below in Figure 3 concluding that the B functionality was met. 

![B Functionality Results](images/B_Functionality.JPG)
##### Figure 3: B Functionality Test Case Results

### Observations and Conclusions
The purpose of this lab was to create a program that could decrypt an encrypted message given a key. I believe that I have completed and met this purpose. Although I did not meet the A functionality, I feel confident that my program could be given the key, regardless of the length, and could have encrypted the message given. The most valuable thing that I have gained from this lab was understanding how to use subroutines in assembly code. I also learned that I need to return to where ever the subroutine was called instead of jumping back there. Because of this, I will now be able to implement subroutines on the rest of my labs. 

### Documentation
None
