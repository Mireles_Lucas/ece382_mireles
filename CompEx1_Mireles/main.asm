;-------------------------------------------------------------
; ComEx1_Mireles - Introduction to the MSP430 and Code Composer Studio
; Capt Jeff Falkinburg, USAF / 23 Aug 2016 / 23 Aug 2016
;
; This program is a demonstration of using the CCS IDE to
; program, assemble, flash, and debug the MSP430.
;-------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
; !!!!!!!!!!!THis is the code for Problem Number 5!!!!!!!!!!!!!!!!
;-------------------------------------------------------------------------------
            mov.w   #0x0200, r9
            mov.w   #0x01ff, r10

            mov.w   #0, r11

loop        tst     r10
            jz      forever

			mov.w   r11, 0(r9)
			incd 	r11
            incd    r9
            dec     r10

            jmp     loop

forever     jmp     forever
;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
