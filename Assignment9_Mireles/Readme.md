#Assignment 9

###Lucas Mireles 

I chose to use the timer interrupt subsystem for my delay on my bouncy ball (B_Functionality). 

###Answers:
- I am using a 80ms delay. This was the time used for the delay in Lab 4. In order to achieve 80 ms, I first selected my clock to use the SMCLK. I then ensured that my clock was using MC1_2 in order to count up to the calculated TA0CCR0 value. In order to achieve the correct value to set TA0CCR0 to, I used the following equation. To ensure that the TA0CCR0 value didn't exceed 0xFFFF I chose to set ID_2 in order to divide the value by a factor of 4. 

############`(1x10^6 clks/1 sec) x (1 cnt/4clks)  x (0.080 sec/1 TAR rollover) = 0x4E20` 


- In order to test that my function worked, I took out my original delay functions that delayed for 80 ms and replaced them with the interrupt. After debugging and running the program, the bouncy ball worked just the same as before. 