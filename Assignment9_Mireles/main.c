/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: 25 Oct 16
Course: ECE 382
File: main.c
Event: Assignment 9

Purp: Includes functions to create a ball, move the ball, and detect
collisions with the screen edges. ALso has the new delay function required for assignment 9

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>
#include "lab4.h"

extern void initMSP();
extern void initLCD();
extern void Delay160ms();
extern void Delay40ms();
extern void clearScreen();
extern void drawBox(unsigned int x, unsigned int y, unsigned int color );
extern void drawPaddle(unsigned int x, unsigned int y, unsigned int color );

#define		TRUE			1
#define		FALSE			0
#define		UP_BUTTON		(P2IN & BIT2)
#define		DOWN_BUTTON		(P2IN & BIT1)
#define		LEFT_BUTTON		(P2IN & BIT0)
#define		RIGHT_BUTTON	(P2IN & BIT3)
#define		AUX_BUTTON		(P1IN & BIT3)

char flag = 0; // indication for timer being done
void requiredFunctionality(){
	unsigned int COLOR = 0x4416;
	int x, y;
	x=5;
	y=5;
	drawBox(x, y, COLOR);

	while(1) {
		Delay160ms();
		if(UP_BUTTON == 0){
			y -= 10;
			if(y<=5){y=5;}
			drawBox(x, y, COLOR);
		}
		else if(DOWN_BUTTON == 0){
			y += 10;
			if(y>=SCREEN_HEIGHT-15){y=SCREEN_HEIGHT-15;}
			drawBox(x, y, COLOR);
		}
		else if(RIGHT_BUTTON == 0){
			x+= 10;
			if(x>=SCREEN_WIDTH-15){x=SCREEN_WIDTH-15;}
			drawBox(x, y, COLOR);
		}
		else if(LEFT_BUTTON == 0){
			x-= 10;
			if(x<=5){x=5;}
			drawBox(x, y, COLOR);
		}
		else if(AUX_BUTTON == 0){
			if(COLOR == 0){
				COLOR = 0x4416;
			}
			else{
				COLOR = 0;
			}
		}
	}
}

BFunctionality(){
	unsigned int COLOR = 0x4416;
	Ball myBall = createBall( XPOS_START, YPOS_START, XVEL_START, YVEL_START, RADIUS );
	TA0CTL |= TAIE;
	TA0CTL |= MC_1;
	while (1){
		flag = 0;
		drawBox(myBall.pos.x, myBall.pos.y, COLOR);
//		int i;
		while(flag !=1); // i think this is right

//		Delay40ms();
//		Delay40ms();
		drawBox(myBall.pos.x, myBall.pos.y, 0);
		myBall = moveBall (myBall);
		if(bottomCollision(myBall)){
			myBall.vel.y *= -1;
		}
	}
}

AFunctionality(){
	unsigned int COLOR = 0x4416;
	int GAMEOVER = FALSE;
	int score = 0;
	int paddleX = SCREEN_WIDTH/2 - 15;
	int paddleY = SCREEN_HEIGHT - 20;
	Ball myBall = createBall( XPOS_START, YPOS_START, XVEL_START, YVEL_START, RADIUS );
	while(!GAMEOVER){
		drawBox(myBall.pos.x, myBall.pos.y, COLOR);
		drawPaddle(paddleX, paddleY, COLOR);
		Delay40ms();
		Delay40ms();
		drawBox(myBall.pos.x, myBall.pos.y, 0);
		drawPaddle(paddleX, paddleY, 0);
		if(!LEFT_BUTTON){
			paddleX -= 5;
			if(paddleX <= 0){paddleX = 0;}
		}
		if(!RIGHT_BUTTON){
			paddleX += 5;
			if(paddleX >= SCREEN_WIDTH - PADDLE_LENGTH){paddleX = SCREEN_WIDTH - PADDLE_LENGTH;}
		}
		myBall = moveBall(myBall);
		if(paddleCollision(myBall, paddleX, paddleY)){
			myBall.vel.y *= -1;
			score += 1;
		}
		if(bottomCollision(myBall)){
			GAMEOVER = TRUE;
		}
	}
}

//void Delay_1ms(){
//    BCSCTL1 = CALBC1_8MHZ;      // Set SMCLK 8 MHz
//    DCOCTL = CALDCO_8MHZ;
//    TA0CCR0 = 8000 - 1;                    // Set the interval // using 8000 from the equation
//    // 8*10^6 clks/sec * 1cnt/1 clks * .001 sec/1 Tar rollover = 8000
//    TA0CTL &= ~TAIFG;                     // Clear any rollover flag
//    TA0CTL |= ID_0 | TASSEL_2 | MC_1;     // Important Timer stuff!
//
//    while ((TA0CTL & TAIFG) == 0); // Polling timer flag
//    TA0CTL &= ~TAIFG;              // Clear rollover flag
//    return;
//}

#pragma vector=TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR(){
	TA0CTL &= ~TAIFG;
	flag = 1;
}
void main() {
	// === Initialize system ================================================
	IFG1=0; /* clear interrupt flag1 */
	WDTCTL= WDTPW+WDTHOLD; /* stop WD */

	 TA0CTL |= TACLR;
	 TA0CTL &= ~TAIFG;
	 TA0CCR0 = 0x4E20; // value to make delay equal 80 ms.
	 // 1*10^6 clks/sec * 1cnt/4 clks * .080 sec/1 Tar rollover = 0x4e20
	 TA0CTL |= ID_2 | TASSEL_2 | MC_0;
	 __enable_interrupt();
	initMSP();
	Delay160ms();
	initLCD();
	Delay160ms();
	clearScreen();

//	requiredFunctionality();
	BFunctionality();
//	AFunctionality();
}
