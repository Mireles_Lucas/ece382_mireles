/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 13 Oct 16
Course: ECE 382
File: simpleLab4.c
Event: Lab 4

Purp: Prelab activity for Lab 4

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>

typedef	unsigned short int16;

int16 func(int16 w, int16 x, int16 y, int16 z);

void main() {

	int16	a,b,c,d,e;

	a=1;
	b=2;
	c=3;
	d=4;

	while(1) {

		e = func(a,b,c,d);
		d = e+5;
		c = d+1;
		b = c+1;
		a = b+1;
	}
}


int16 func(int16 w, int16 x, int16 y, int16 z) {

	int16 sum;

	sum = w+x+y+z;
	sum = sum >> 2;

	return(sum);

}
