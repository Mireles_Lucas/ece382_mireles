/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: 10 October 2016
Course: ECE 382
File: pong.h
Event: Assignment 7 - Pong

Purp: header file for my functions create and move ball along with defining my structures to be used

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef PONG_H_
#define PONG_H_
#define TRUE 1
#define FALSE 0
#define SCREEN_WIDTH 500
#define SCREEN_HEIGHT 500

typedef struct {
    int x;
    int y;
} vector2d_t;

typedef struct {
    vector2d_t position;
    vector2d_t velocity;
    unsigned char radius;
} ball_t;

ball_t createBall(int xPos, int yPos, int xVel, int yVel, unsigned char radius); //creates a ball

ball_t moveBall(ball_t ballToMove); // updates the balls position based off of its velocity

#endif /* PONG_H_ */
