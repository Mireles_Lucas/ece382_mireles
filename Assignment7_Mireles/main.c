/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: 10 October 2016
Course: ECE 382
File: main.c
Event: Assignment 7 - Pong

Purp: Calls create ball and move ball

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430.h>
#include "pong.h"



/*
 * main.c
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
    ball_t myBall;
    int counter; // counter for my loop
	myBall = createBall(485,485,-5,5,10); //screen is 500x500 for tests
	for (counter = 0; counter<11; counter++){
		myBall = moveBall(myBall);
	}
	while(1);
}
