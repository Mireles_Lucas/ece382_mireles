/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: 10 October 2016
Course: ECE 382
File: pong.c
Event: Assignment 7 - Pong

Purp: Where the actual functions are for creating the ball and moving the ball along with all of the collision functions for checking the boarders.

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/*typedef struct {
    int x;
    int y;
} vector2d_t;

typedef struct {
    vector2d_t position;
    vector2d_t velocity;
    unsigned char radius;
} ball_t;
*/
#include "pong.h"

ball_t createBall(int xPos, int yPos, int xVel, int yVel, unsigned char radius){ //creates the ball using nested structures
	vector2d_t pos;
	vector2d_t vel;
	ball_t b1;
	pos.x = xPos;
	pos.y = yPos;
	vel.x = xVel;
	vel.y = yVel;
	b1.position = pos;
	b1.velocity = vel;
	b1.radius = radius;
	return b1;
}

ball_t moveBall(ball_t ballToMove){	//moves the ball based on its velocity
	ballToMove.position.x += ballToMove.velocity.x;
	ballToMove.position.y += ballToMove.velocity.y;
	if (Collision_left(ballToMove) == TRUE){
		ballToMove.velocity.x *= -1;
		ballToMove.position.x += 2*ballToMove.velocity.x;
		}
	if (Collision_right(ballToMove) == TRUE){
			ballToMove.velocity.x *= -1;
			ballToMove.position.x += 2*ballToMove.velocity.x;
		}
	if (Collision_up(ballToMove) == TRUE){
				ballToMove.velocity.y *= -1;
				ballToMove.position.y += 2*ballToMove.velocity.y;
		}
	if (Collision_down(ballToMove) == TRUE){
				ballToMove.velocity.y *= -1;
				ballToMove.position.y += 2*ballToMove.velocity.y;
		}
	return ballToMove;
}

// the following collision functions checks to see if the ball has hit the boundary and then chnages the direction of the velocity
char Collision_left(ball_t b){
	if (b.position.x - b.radius < 0){
		return TRUE;
	}
	else{return FALSE;}

}
char Collision_right(ball_t b){
	if (b.position.x + b.radius > SCREEN_WIDTH){
			return TRUE;
		}
		else{return FALSE;}

}
char Collision_up(ball_t b){
	if (b.position.y - b.radius < 0){
			return TRUE;
		}
		else{return FALSE;}

}
char Collision_down(ball_t b){
	if (b.position.y + b.radius > SCREEN_HEIGHT){
			return TRUE;
		}
		else{return FALSE;}

}



