#  Assignment 7 - Pong

## By Lucas Mireles 

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Debugging](#debugging)
3. [Testing methodology or results](#testing-methodology-or-results)
4. [Documentation](#documentation)
 
### Objectives or Purpose 
Write a C program that implements a subset of the functionality of the video "pong" game.

- Define the height/width of the screen as constants.
- Create a structure that contains the ball's parameters (x/y position, x/y velocity, radius). 
- Make a function that creates a ball based on parameters passed into it.
- Make another function that updates the position of the ball (input is a ball structure, output is the updated ball structure).
    - This function must handle the "bouncing" of the ball when it hits the edges of the screen.
    - When it hits an edge, you flip the sign on the x or y velocity to make the ball move a different direction.
    - This function should call four other "collision detection" functions; one for each of the screen edges.
    - The "collision detection" functions return a `char` (1 for true, 0 for false - `#define` these values) to indicate whether or not there is a collision.
- Create three separate files: header, implementation, and your `main.c`.

### Debugging 
The biggest problem I had was that I was that I initially had my createBall function in the same loop that my update ball was in so I was continuously initializing the ball right after I updated its position.

###Testing and Answers to Lab Questions 
Below are screen shots that I took of my variables after building and initializing them. The width and height of the screen is 500 x 500 for testing purposes. I took a screen shot of the ball's position and velocity both on the boarder and after the collision of the boarder for all 4 sides.

![](images/on_right.JPG)
###### On the right boarder 

![](images/after_right.JPG)
###### After the right collision 

![](images/on_left.JPG) 
###### On the left boarder
 
![](images/after_left.JPG)
###### After the left collision 

![](images/on_top.JPG)
###### On the top boarder 

![](images/after_top.JPG)
###### After the top collision 

![](images/on_bottom.JPG)
###### On the bottom boarder 

![](images/after_bottom.JPG)
###### After the bottom collision 

###Questions 
1. I verified my code worked properly by looking at the variables in the debugging section and created a loop that would move the ball 10 times and I would calculate where I thought the ball should be based on its velocity compared to the end position of the ball after the loop was complete.
2. By putting the word static in front of the function, this prevents main.c from being able to have access to this function. 
### Documentation
None
