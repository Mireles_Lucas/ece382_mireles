/*--------------------------------------------------------------------
Name:Lucas Mireles
Date:October 5, 2016
Course: ECE 382
File:Assignemt6_Mireles
HW: Assignment 6

Purp:Intro to C

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h> 
#define THRESHOLD 0x38

/*
 * main.c
 */
void main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    unsigned char val1 = 0x40;		//intializing all the variables
    unsigned char val2 = 0x35;
    unsigned char val3 = 0x42;
    unsigned char result1 = 0;
    volatile unsigned char result2 = 0;
    volatile unsigned char result3 = 0;

	if (val1 > THRESHOLD){		//checking to see if val1 is great that the threshold
		unsigned char next;
		unsigned char first = 0;
		unsigned char second = 1;
		unsigned char i = 0;
		for (i = 0; i < 10; i++){
			if (i <= 1){
				next = i;
			}
			else{
				next = first + second;
				first = second;
				second = next;
			}
		}
		result1 = next;
	}
	if (val2 > THRESHOLD){ //checking to see if val2 is great that the threshold
		result2 = val2;
	}
	if (val3 > THRESHOLD){ //checking to see if val3 is great that the threshold
		result3 = val2 - 0x10;
	}
	while (1){};
}
