#  Assignment 6 - Your First C Program

## By Lucas Mireles 

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Subroutines](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Observations and Conclusions](#observations-and-conclusions)
8. [Documentation](#documentation)
 
### Objectives or Purpose 
Write a C program that does the following:

Creates three unsigned chars (val1, val2, val3). Initializes them to 0x40, 0x35, and 0x42.
Creates three unsigned chars (result1, result2, result3). Initializes them to 0.
Checks each number against a threshold value of 0x38.
If val1 is greater than the threshold value, stores the 10th Fibonacci number in result1 by using a for loop.
If val2 is greater than the threshold value, stores 0xAF to result2.
If val3 is greater than the threshold value, subtracts 0x10 from val2 and stores the result into result3.

Additional Requirements:

The threshold value must be a properly defined constant (refer to the lesson notes if you are confused by this requirement).
Comment your code. In particular, provide a good file header (example provided in lesson notes).

### Debugging 
The biggest problem I had was that I was checking my Fibonacci answer when it was in hex so I continued to think that it was wrong.

###Testing and Answers to Lab Questions 
Below are screen shots that I took of my variables after building and initializing them and then the results after the program was run.

![](images/before.JPG)
###### Before 

![](images/after.JPG)
###### Before
1. My variables are stored in Ram more specifically on the stack
2. I know this because the values begin being stored at 0x400 and start moving down in RAM. 
### Documentation
None
