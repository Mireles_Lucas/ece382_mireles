/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: 8 Dec 16
Course: ECE 382
File: lab7.c
Event: Lab 7

Purp: source file that contains all the actual functions being called by main

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/


#include <msp430.h>
#include "Lab7.h"



void initMSP430(void){
	WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer

				P2DIR |= BIT1;                // TA1CCR1 on P2.1
				P2SEL |= BIT1;
	//	        P2SEL &= ~BIT1;				// TA1CCR1 on P2.1
				P2OUT &= ~BIT1;

				P2DIR |= BIT4;                // TA1CCR2 on P2.4
				P2SEL |= BIT4;                // TA1CCR2 on P2.4
	//	        P2SEL2 &= ~BIT4;
				P2OUT &= ~BIT4;

				P2DIR |= BIT2|BIT3;		//sets up 3 GPIO (1.0 is enable, 2.3 is left direction, 2.2 is right direction)
				P2SEL &= ~ (BIT2|BIT3);
				P2OUT &= ~ (BIT2|BIT3);
				P1DIR |= BIT0;
				P1OUT &= ~BIT0;



		        P1DIR &= ~BIT4;      				//set up as GPIO ECHO
		        P1SEL &= ~BIT4;
		        P1IES &= ~BIT4;
		        P1IFG &= ~BIT4;						// Clear any interrupt flag on P1.4

				P2DIR |= BIT7;						// Set up P2.7 as GPIO for sensor Trigger
				P2SEL &= ~BIT7;						// Once again, this take three lines
			    P2SEL2 &= ~BIT7;					// to properly do
			    P2OUT &= ~BIT7;

				P1DIR |= BIT0|BIT6;					// Set LEDs as outputs
				P1SEL &= ~(BIT0|BIT6);
				P1SEL2 &= ~(BIT0|BIT6);
				P1OUT &= ~(BIT0|BIT6);				// And turn the LEDs off

		        TA0CTL |= TASSEL_2|MC_1|ID_0;		// TA0, PWM for servo, US
		        TA0CTL &= ~TAIE;

		        P1DIR |= BIT2;						// PWM for servo
		       	P1SEL |= BIT2;						//
		       	P1SEL2 &= ~BIT2;					//
		       	P1OUT  &= ~BIT2;

		       	TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK

				TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
				TA1CCR1 = 305;                // set duty cycle to 50%
				TA1CCTL1 |= OUTMOD_7;        // set TACCTL1 to Set / Reset mode

				TA1CCR2 = 325;                // set duty cycle to 50%
				TA1CCTL2 |= OUTMOD_7;        // set TACCTL2 to Set / Reset mode

				TA0CCR0 = 20000;					// create a 20ms roll-over period
				TA0CCTL1 |= OUTMOD_7;

		       	_enable_interrupt();
		       	return;
}

void turnleft(){
				TA0CCR0 = 20000;
				TA0CCR1 = 2500;
				TA0CTL |= MC_1;
//				TA0CCTL1 |= OUTMOD_7;
				P1SEL |= BIT2;
				__delay_cycles(500000);
				P1SEL &= ~BIT2;
//				TA0CCTL1 |= OUTMOD_0;
				TA0CTL &= ~MC_1;
}
void turnright(){
				TA0CCR0 = 20000;
				TA0CCR1 = 550;
				TA0CTL |= MC_1;
				P1SEL |= BIT2;
//				TA0CCTL1 |= OUTMOD_7;
				__delay_cycles(500000);
				P1SEL &= ~BIT2;
//				TA0CCTL1 |= OUTMOD_0;
				TA0CTL &= ~MC_1;
}

void turnfront(){
				TA0CCR0 = 20000;
				TA0CCR1 = 1500;
				TA0CTL |= MC_1;
				P1SEL |= BIT2;
//				TA0CCTL1 |= OUTMOD_7;
				__delay_cycles(200000);
//				TA0CCTL1 |= OUTMOD_0;
				P1SEL &= ~BIT2;
				TA0CTL &= ~MC_1;
}
void checkleft(){
				turnleft();
				get_distance();
				__delay_cycles(200000);
//				if (dist < 12){
//					P1OUT |= BIT0;
//				}
//				__delay_cycles(2000000);
				P1OUT &= ~BIT0;
}

void checkright(){

				turnright();
				get_distance();
				__delay_cycles(1000000);

//				if (dist < 12){
//					P1OUT |= BIT6;
//					P1OUT &= ~BIT0;
//				}
//				__delay_cycles(2000000);
				P1OUT &= ~BIT6;

}
void checkfront(){

				turnfront();

				get_distance();
//				if (dist < 12){
//					P1OUT |= BIT0|BIT6;
//				}
//				__delay_cycles(1000000);
				P1OUT &= ~(BIT0|BIT6);


}

void get_distance(){
				TA0CCR0 = 50000;
				P2OUT |= BIT7;
				__delay_cycles(10);
				P2OUT &= ~BIT7;
				P1IE |= BIT4;
				__delay_cycles(70000);
				P1IE &= ~BIT4;
				TA0CCR0 = 20000;
}
void forward(){ //moves the robot forward
	// Forward

	P1OUT |= BIT0;  //set enable
	__delay_cycles(500000);
	P1OUT &= ~ BIT0;
	return;
}
void backward(){ //moves the robot backward
	TA1CCTL1 |= OUTMOD_3;
	P1OUT |= BIT0;  //set enable
	P2OUT |= BIT2|BIT3;
	__delay_cycles(1000000);
	P2OUT &= ~ (BIT2|BIT3);
	P1OUT &= ~ BIT0;
	TA1CCTL1 |= OUTMOD_7;
	return;
}
void left(){ //moves the robot slightly left
	__delay_cycles(1000000);
	P2OUT |= BIT3;
	P1OUT |= BIT0;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	__delay_cycles(50000);
	P2OUT &= ~ BIT3;
	P1OUT &= ~ BIT2;
	return;
}
void right(){ //moves the robot slighty right
	__delay_cycles(1000000);
	P2OUT |= BIT2;
	P1OUT |= BIT2;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	__delay_cycles(50000);
	P2OUT &= ~ BIT2;
	P1OUT &= ~ BIT0;
	return;
}
void left90(){ //moves the robot 90 degrees to the right
	//90 left
	__delay_cycles(1000000);
	P2OUT |= BIT2;
	P1OUT |= BIT0;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	__delay_cycles(280000);
	P2OUT &= ~ (BIT2|BIT3);
	P1OUT &= ~ BIT0;
	return;
}
void right90(){ //moves the robot 90 degrees to the left
	 //90 right
	P2OUT |= BIT3;
	P1OUT |= BIT0;  //set enable  (might want to make TA1CCR1 reset to not move the wheel)
	__delay_cycles(285000);
	P2OUT &= ~ (BIT2|BIT3);
	P1OUT &= ~ BIT0;
	return;
}
