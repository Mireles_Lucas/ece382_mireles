/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: 8 Dec 16
Course: ECE 382
File: main.c
Event: Lab 7

Purp: Calls the function to move the robot and checks the distance

Doc: C2C Hanson helped figure out why my servo was not turning once i had the echo interrupt working.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h>
#include "Lab7.h"
//-----------------------------------------------------------------------------------------------
// does this work?
//-----------------------------------------------------------------------------------------------
int8	newIrPacket = FALSE;
int16	packetData[48];
int8	packetIndex = 0;
int32	irPacket = 0;
int16 	echoPacket = 0;

static unsigned int DISTANCE = 0;

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
char ultrasonic_flag = 0;
int main(void) {
	initMSP430();
	while(1) {

				checkfront(DISTANCE);
//
				if (DISTANCE < 6){
					checkleft();
					if (DISTANCE > 10){
						left90();
					}
					else{
						right90();
					}

				}
				forward();

	}
}


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

#pragma vector = PORT1_VECTOR
__interrupt void distance(void){
	if (P1IN & BIT4){
		TA0R = 0;
		TA0CTL |= MC_1;
		P1IES |= BIT4;
	}
	else{
		echoPacket = TA0R;
		TA0CTL &= ~MC_1;
		DISTANCE = (echoPacket / 148) - 2;
		P1IES &= ~BIT4;

	}
	P1IFG &= ~BIT4;

}
