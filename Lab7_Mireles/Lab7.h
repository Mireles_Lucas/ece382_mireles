/*--------------------------------------------------------------------
Name: Lucas Mireles
Date: 8 Dec 16
Course: ECE 382
File: lab7.h
Event: Lab 7

Purp: header file for lab 7

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#ifndef LAB7_H_
#define LAB7_H_

void checkleft();
void checkright();
void checkfront();
void servo();
void forward();
void backward();
void left();
void right();
void right90();
void left90();
void initMSP430(void);
void required_functionality(void);
void get_distance(void);

typedef		unsigned char		int8;
typedef		unsigned short		int16;
typedef		unsigned long		int32;
typedef		unsigned long long	int64;


#define		TRUE				1
#define		FALSE				0


//-----------------------------------------------------------------
// Function prototypes found in lab5.c
//-----------------------------------------------------------------
void initMSP430();
__interrupt void pinChange (void);
__interrupt void timerOverflow (void);
__interrupt void distance (void);


//-----------------------------------------------------------------
// Each PxIES bit selects the interrupt edge for the corresponding I/O pin.
//	Bit = 0: The PxIFGx flag is set with a low-to-high transition
//	Bit = 1: The PxIFGx flag is set with a high-to-low transition
//-----------------------------------------------------------------

#define		IR_PIN			(P2IN & BIT6)
#define		HIGH_2_LOW		P2IES |= BIT6
#define		LOW_2_HIGH		P2IES &= ~BIT6

#define		averageLogic0Pulse	0x0235
#define		averageLogic1Pulse	0x067C
#define		averageStartPulse	0x1151
#define		minLogic0Pulse		averageLogic0Pulse - 100
#define		maxLogic0Pulse		averageLogic0Pulse + 100
#define		minLogic1Pulse		averageLogic1Pulse - 100
#define		maxLogic1Pulse		averageLogic1Pulse + 100
#define		minStartPulse		averageStartPulse - 100
#define		maxStartPulse		averageStartPulse + 100

#define		PWR		0x20DF10EF
#define		OK		0x20DF22DD
#define		LEFT	0x20DFE21D
#define		RIGHT	0x20DF12ED
#define		UP		0x20DFA25D
#define		DOWN	0x20DF629D
#define		YELLOW	0x20DF4AB5
#define		BLUE	0x20DFCA35
#define		RED		0x20DF2AD5
#define		GREEN	0x20DFAA55

#endif /* LAB6_H_ */

