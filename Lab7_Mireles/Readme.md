# Lab 7 and 8 - A/D Conversion - "The Trap Bot"

## By C2C Lucas Mireles

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
3. [Hardware schematic](#hardware-schematic)
4. [Debugging](#debugging)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Lab 7 Objectives and Purpose 

This lab is designed to assist you in learning the concepts associated with the input capture features for your MSP430.  A single ultrasonic rangefinder is attached to a servo motor that can rotate.  You will program your MSP430 to use the rangefinder to determine whether your mobile robot is approaching a wall in front or on one of its sides.  The skills you will learn from this lab will come in handy in the future as you start interfacing multiple systems.

-Required Functionality

Use the Timer_A subsystem to light LEDs based on the presence of a wall.  The presence of a wall on the left side of the robot should light LED1 on your Launchpad board.  The presence of a wall next to the right side should light LED2 on your Launchpad board.  A wall in front should light both LEDs.  Demonstrate that the LEDs do not light until the sensor comes into close proximity with a wall.

### Lab 8 Objectives and Purpose 
During this lab, you will combine the previous laboratory assignments and program your robot to autonomously navigate through a maze. On the last day of the lab, each section will hold a competition to see who can solve the maze the fastest. The fastest time in ECE 382 will have their name engraved on a plaque in the lab. Believe it or not, the main goal of this lab is for you to have some fun with computer engineering!


![](images/maze.JPG)


### Prelab

1.  How fast does the signal pulse from the sensors travel?
    340 m/s
2.  If the distance away from the sensor is 1 in, how long does it take for the
    sensor pulse to return to the sensor? 296 us

	 1 cm? 116 us

3.  What is the range and accuracy of the sensor? 

Range is between 2 cm and 4 m and the accuracy is within .3 cm.

4.  What is the minimum recommended delay cycle (in ms) for using the sensor?  60 ms How does this compare to the "working frequency"?
  
working frequency is 40 kHz so the delay cycle is twice the working period


#### Servo
1.  Fill out the following table identifying the pulse lengths needed for each servo position:

| Servo Position | Pulse Length (ms) | Pulse Length (counts) |
|:--------------:|:-----------------:|:---------------------:|
| Left           |         1         |          1000         |
| Middle         |          1.5      |          1500         |
| Right          |          2        |          2000         |
    
<br>



### Hardware schematic
Below is the schematic of the breadboard, MSP430, and motors and how they will be connected and supplied power.

![](images/Schematic.JPG)

### Flowchart 



![](images/Flowchart.JPG)

![](images/Lab8Flow.JPG)
#####Flowchart for Lab 8 Prelab

### Debugging
The biggest issue I faced during this lab was having my servo turn when and where I wanted to once the ultrasonic sensor was set up and working. The reason this was such an issue was because I was attempting to use the same pin that the Green LED was on for my PWM signal to move the servo. After quite some time of being hard headed and still trying to make this work, I decided to put my PWM signal on another pin instead. Once this change was made, the servo began to act correctly and listen nicely.
### Testing methodology or results

##### Functionality
The required functionality for lab 7 can be found in the objective for Lab 7 above.

The functionality for lab 8 was to a program that autonomously navigates my robot through a maze (Figure 1) while meeting the following requirements:

1. Your robot must always start at the home position.
2. Your robot is considered successful only if it finds one of the three exits and moves partially out of the maze.
3. A large portion of your grade depends on which door you exit.
  1. Door 1 - Required Functionality
  2. Door 2 - B Functionality
  3. Door 3 - A Functionality
    	1. **You cannot hit a wall!**
  4. Bonus!  Navigate from the A door back to the entrance using the same algorithm.
   	 1. **You cannot hit a wall!**
4. Your robot must solve the maze in less than three minutes.
5. Your robot will be stopped if it touches the wall more than twice.
6. Your robot must use the ultrasonic sensor to find its path through the maze.


Below can be found the video that demonstrates required functionality for both lab 7 and lab 8 considering the fact that lab 7 was to demonstrate that the sensors work appropriately which navigating through the maze does.
[Proof of Functionality](https://www.youtube.com/watch?v=mmcKX7HTLls)


![](images/TrapBot.jpg)

######Final Trap Bot

### Observations and Conclusions
The objective of this lab was to use to use PWMs in order to control the speed and direction of the wheels of the robot to control its movement as well as use the servo and ultrasonic sensor to navigate through the maze successfully. Because I was able to accomplish both A and Bonus functionality, I believe that I have successfully met this objective.  

I will use the experience gained from this lab in future courses such as 383 in order to accomplish tasks that require interrupts using sensors as well as PWMs.

### Documentation
None.